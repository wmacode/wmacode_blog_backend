<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['title', 'description', 'content', 'category_id'];

    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Get the category that owns the comment.
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
}
