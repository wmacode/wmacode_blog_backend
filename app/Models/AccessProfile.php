<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessProfile extends Model
{
    protected $fillable = ['type', 'name'];

    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Get the users for the blog post.
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
