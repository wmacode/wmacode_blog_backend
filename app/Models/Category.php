<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = ['name'];

    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Get the articles for the blog post.
     */
    public function articles()
    {
        return $this->hasMany('App\Models\Articles');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Category', 'parent_id');
    }


}
