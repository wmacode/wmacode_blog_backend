<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AccessProfile;

class AccessProfileController extends Controller
{
    /**
     * Return a listing of the article.
     *
     * @return \Illuminate\Http\Response
     */
    public function findAll()
    {
        try{
            $accessProfiles = AccessProfile::all();
            return response()->json($accessProfiles, 200);
    	} catch(\Exception $e){
    		return response()->json(setErrorApi('Não foi possivel encontrar perfis de acesso.', $e), 500);
    	}
    }

    /**
     * Return a specific article.
     *
     * @return \Illuminate\Http\Response
     */
    public function find($id)
    {
        try{
            $accessProfile = AccessProfile::findOrfail($id);
            return response()->json($accessProfile, 200);
    	} catch(\Exception $e){
    		return response()->json(setErrorApi('Não foi possivel encontrar perfil acesso.', $e), 500);
    	}
    }

    /**
     * Store a newly created article in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try{
            AccessProfile::create($request->all());
            return response()->json(['msg' => 'Perfil de acesso cadastrado com sucesso'], 201);
    	} catch(\Exception $e){
            return response()->json(setErrorApi('Não foi possivel criar novo perfil de acesso.', $e), 500);
    	}
    }

    /**
     * Update the specified article in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $accessProfile = AccessProfile::findOrFail($id);
            $accessProfile->fill($request->all);
            return response()->json(['msg' => 'Perfil de acesso editado com sucesso'], 201);
        } catch(\Exception $e){
            return response()->json(setErrorApi('Não foi possivel editar perfil de acesso.', $e), 500);
        }
    }

    /**
     * Remove the specified article from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove($id)
    {
        try{
            $accessProfile = AccessProfile::findOrFail($id);
            $accessProfile->delete();
            return response()->json(['msg' => 'Perfil de acesso excluído com sucesso'], 201);
        } catch(\Exception $e){
            return response()->json(setErrorApi('Não foi possivel excluir perfil de acesso.', $e), 500);
        }
    }
}

