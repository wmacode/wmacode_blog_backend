<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article;

class ArticleController extends Controller
{
    /**
     * Return a listing of the article.
     *
     * @return \Illuminate\Http\Response
     */
    public function findAll()
    {
        try{
            $articles = Article::all();
            return response()->json($articles, 200);
    	} catch(\Exception $e){
    		return response()->json(setErrorApi('Não foi possivel encontrar artigos.', $e), 500);
    	}
    }

    /**
     * Return a specific article.
     *
     * @return \Illuminate\Http\Response
     */
    public function find($id)
    {
        try{
            $articles = Article::findOrfail($id);
            return response()->json($articles, 200);
    	} catch(\Exception $e){
    		return response()->json(setErrorApi('Não foi possivel encontrar artigo.', $e), 500);
    	}
    }

    /**
     * Store a newly created article in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try{
            Article::create($request->all());
            return response()->json(['msg' => 'Artigo cadastrado com sucesso'], 201);
    	} catch(\Exception $e){
            return response()->json(setErrorApi('Não foi possivel criar novo artigo.', $e), 500);
    	}
    }

    /**
     * Update the specified article in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $category = Article::findOrFail($id);
            $category->fill($request->all);
            return response()->json(['msg' => 'Artigo editado com sucesso'], 201);
        } catch(\Exception $e){
            return response()->json(setErrorApi('Não foi possivel editar artigo.', $e), 500);
        }
    }

    /**
     * Remove the specified article from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove($id)
    {
        try{
            $article = Article::findOrFail($id);
            $article->delete();
            return response()->json(['msg' => 'Artigo excluído com sucesso'], 201);
        } catch(\Exception $e){
            return response()->json(setErrorApi('Não foi possivel excluir artigo.', $e), 500);
        }
    }
}

