<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use app\Models\Categor;
use App\Models\Category;
use App\Exceptions\Handler;

class CategoryController extends Controller
{
    /**
     * Return a listing of the category.
     *
     * @return \Illuminate\Http\Response
     */
    public function findAll()
    {
        try{
            $categories = Category::all();
            return response()->json($categories, 200);
    	} catch(\Exception $e){
    		return response()->json(setErrorApi('Não foi possivel encontrar categorias.', $e), 500);
    	}
    }

    /**
     * Return a specific category.
     *
     * @return \Illuminate\Http\Response
     */
    public function find($id)
    {
        try{
            $category = Category::findOrFail($id);
            return response()->json($category, 200);
    	} catch(\Exception $e){
    		return response()->json(setErrorApi('Não foi possivel encontrar categoria.', $e), 500);
    	}
    }

    /**
     * Store a newly created category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(CategoryRequest $request)
    {
        try{
            Category::create($request->all());
            return response()->json(['msg' => 'Categoria cadastrado com sucesso'], 201);
    	} catch(\Exception $e){
            return response()->json(setErrorApi('Não foi possivel criar nova categoria.', $e), 500);
    	}
    }

    /**
     * Update the specified category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        try{
            $category = Category::findOrFail($id);
            $category->name = $request->name;
            $category->save();
            return response()->json(['msg' => 'Categoria editada com sucesso'], 201);
        } catch(\Exception $e){
            return response()->json(setErrorApi('Não foi possivel editar categoria.', $e), 500);
        }
    }

    /**
     * Remove the specified category from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove($id)
    {
        try{
            $category = Category::findOrFail($id);
            $category->delete();
            return response()->json(['msg' => 'Categoria excluída com sucesso'], 201);
        } catch(\Exception $e){
            return response()->json(setErrorApi('Não foi possivel excluir categoria.', $e), 500);
        }
    }
}
