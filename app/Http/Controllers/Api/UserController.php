<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    /**
     * Return a listing of the article.
     *
     * @return \Illuminate\Http\Response
     */
    public function findAll()
    {
        try{
            $users = User::all();
            return response()->json($users, 200);
    	} catch(\Exception $e){
    		return response()->json(setErrorApi('Não foi possivel encontrar usuários.', $e), 500);
    	}
    }

    /**
     * Return a specific article.
     *
     * @return \Illuminate\Http\Response
     */
    public function find($id)
    {
        try{
            $user = User::findOrfail($id);
            return response()->json($user, 200);
    	} catch(\Exception $e){
    		return response()->json(setErrorApi('Não foi possivel encontrar usuário.', $e), 500);
    	}
    }

    /**
     * Store a newly created article in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try{
            User::create($request->all());
            return response()->json(['msg' => 'Usuário cadastrado com sucesso'], 201);
    	} catch(\Exception $e){
            return response()->json(setErrorApi('Não foi possivel criar novo usuário.', $e), 500);
    	}
    }

    /**
     * Update the specified article in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $user = User::findOrFail($id);
            $user->fill($request->all);
            return response()->json(['msg' => 'Usuário editado com sucesso'], 201);
        } catch(\Exception $e){
            return response()->json(setErrorApi('Não foi possivel editar usuário.', $e), 500);
        }
    }

    /**
     * Remove the specified article from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove($id)
    {
        try{
            $user = User::findOrFail($id);
            $user->delete();
            return response()->json(['msg' => 'Usuário excluído com sucesso'], 201);
        } catch(\Exception $e){
            return response()->json(setErrorApi('Não foi possivel excluir usuário.', $e), 500);
        }
    }
}

