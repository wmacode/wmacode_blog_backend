<?php

function setErrorApi($message, $e){
    return [
        'success' => false,
        'msg' => $message,
        'error' => [
            'code' => $e->getCode(),
            'message' => $e->getMessage(),
            'file' => $e->getFile(),
            'line' => $e->getLine(),
        ]
    ];
}
