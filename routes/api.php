<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('auth')->namespace('Api')->group(function () {
    Route::post('login', 'AuthController@login');
    Route::middleware('auth:api')->group(function(){
        Route::post('logout',  'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me',      'AuthController@me');
    });
});

Route::get('categories', 'Api\CategoryController@findAll');
Route::get('categories/{id}', 'Api\CategoryController@find');
Route::post('categories', 'Api\CategoryController@create');
Route::put('categories/{id}', 'Api\CategoryController@update');
Route::delete('categories/{id}', 'Api\CategoryController@remove');

Route::get('articles', 'Api\ArticleController@findAll');
Route::get('articles/{id}', 'Api\ArticleController@find');
Route::post('articles', 'Api\ArticleController@create');
Route::put('articles/{id}', 'Api\ArticleController@update');
Route::delete('articles/{id}', 'Api\ArticleController@remove');

Route::get('access_profiles', 'Api\AccessprofileController@findAll');
Route::get('access_profiles/{id}', 'Api\AccessprofileController@find');
Route::post('access_profiles', 'Api\AccessprofileController@create');
Route::put('access_profiles/{id}', 'Api\AccessprofileController@update');
Route::delete('access_profiles/{id}', 'Api\AccessprofileController@remove');

Route::get('users', 'Api\UsersController@findAll');
Route::get('users/{id}', 'Api\UsersController@find');
Route::post('users', 'Api\UsersController@create');
Route::put('users/{id}', 'Api\UsersController@update');
Route::delete('users/{id}', 'Api\UsersController@remove');
