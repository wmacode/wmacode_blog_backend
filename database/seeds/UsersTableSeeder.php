<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'João',
            'email' => 'joao@oi.com',
            'password' => bcrypt('joao10'),
            'access_profile_id' => 1
        ]);

        DB::table('users')->insert([
            'name' => 'josé',
            'email' => 'jose@oi.com',
            'password' => bcrypt('jose10'),
            'access_profile_id' => 2
        ]);
    }
}
